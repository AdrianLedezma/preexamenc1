
const fetchCall = async (nombrepais) => {
    const url = 'https://restcountries.com/v3.1/name/' + nombrepais;

    try {
        const consulta = await fetch(url);
        const datos = await consulta.json();

        document.getElementById('inpCap').value = datos[0].capital;
        
        arrLenguajes = datos[0].languages;
        lenguajesOutput = "";
       
        for (var language in arrLenguajes) {
            if (arrLenguajes.hasOwnProperty(language)) {
                lenguajesOutput += arrLenguajes[language] + " | ";
            }
        }

        document.getElementById('inpLen').value = lenguajesOutput;

    } catch (error) {
        console.error(error);
        alert("Se produjo un error, intente de nuevo.");
    }
};

document.getElementById("btnBuscar").addEventListener("click", function () {
    nombrepais = document.getElementById('inpNombrePais').value;

    if (!nombrepais) {
        alert("Ingrese el nombre de un país");
    } else {
        fetchCall(nombrepais);
    }
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    document.getElementById("inpNombrePais").value = "";
    document.getElementById("inpCap").value = "";
    document.getElementById("inpLen").value = "";
});
