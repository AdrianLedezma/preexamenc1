function busqueda() {
    const idBusqueda = document.getElementById("idBusqueda");
    const id = idBusqueda.value;

    if (isNaN(id) || id.trim() === "") {
        alert("Capture un número válido antes de realizar la búsqueda.");
        return;
    }

    const url = "https://jsonplaceholder.typicode.com/users/" + id;

    axios.get(url)
        .then(response => {
            const datos = response.data;

            let nombre = document.getElementById("inpNombre");
            nombre.value = datos.name;

            let username = document.getElementById("inpUsername");
            username.value = datos.username;

            let email = document.getElementById("inpEmail");
            email.value = datos.email;

            let calle = document.getElementById("inpDomCal");
            calle.value = datos.address.street;

            let numero = document.getElementById("inpDomNum");
            numero.value = datos.address.suite;

            let ciudad = document.getElementById("inpDomCiu");
            ciudad.value = datos.address.city;
        })
        .catch(error => {
            if (error.response && error.response.status === 404) {
                alert("ID incorrecta.");
            } else {
                console.error("Error en la solicitud:", error.message);
            }
        });
}

document.getElementById("btnBuscar").addEventListener("click", function () {
    busqueda();
});

document.getElementById("btnLimpiar").addEventListener("click", function () {
    document.getElementById("idBusqueda").value = "";
    document.getElementById("inpNombre").value = "";
    document.getElementById("inpUsername").value = "";
    document.getElementById("inpEmail").value = "";
    document.getElementById("inpDomCal").value = "";
    document.getElementById("inpDomNum").value = "";
    document.getElementById("inpDomCiu").value = "";
});
